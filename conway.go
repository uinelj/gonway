package main

import "fmt"
import "math/rand"

type conway struct {
  size_x int
  size_y int
  _map []bool
}


func init_conway(size_x int, size_y int) *conway {
  conway := conway{}
  conway.size_x = size_x
  conway.size_y = size_y
	conway._map = make([]bool, size_x*size_y) //new 10*10 conway game
	var rand_number int

	for idx := range conway._map {
		rand_number = rand.Intn(2)
		switch rand_number {
		case 0:
			conway._map[idx] = false
		case 1:
			conway._map[idx] = true

		}
	}
	return &conway
}

func neighbors(conway_game conway, x int, y int) []bool {
	neighbor_range := [3]int{-1, 0, 1}
  cell_neighbors := make([]bool, 0, len(neighbor_range)*len(neighbor_range))

	for _, xrange := range neighbor_range {
		for _, yrange := range neighbor_range {
			if (xrange != 0) && (yrange != 0) {
        cell_neighbors = append(cell_neighbors, conway_game._map[xrange*yrange])
			}
		}
  }
  return cell_neighbors
}

func step(conway_game conway) *conway {
  // var neighbors []bool

  // for idx, cell := range conway_game._map {
  //   fmt.Println("hi")
  // }

  return &conway_game
}
func main() {
	conway := init_conway(10, 10)
	fmt.Println(conway)
	//fmt.Println(add(2, 2))
}
